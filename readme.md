# PHP Crawler - Lumen Framework 

It's a crawler for Black-Inks.org. It will crawl first page of this website and will return a JSON file of all the links specified in those posts.

## Installation

* Clone the project
* Run "composer install" command
* Run www.yoursite.com/api/v1/crawl
* A file will be generated with name "urls.json" in your project's storage > app folder 

## Testing

Simply run phpunit to test whether your request is valid.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
