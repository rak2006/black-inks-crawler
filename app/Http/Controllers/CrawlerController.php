<?php

namespace App\Http\Controllers;


use Goutte\Client;
use Illuminate\Support\Facades\Storage;

class CrawlerController extends Controller
{
    /**
     * Crawls backlinks from the link specified
     *
     * @return Response
     */
    public function index()
    {
        // Variables
        $crawl_url = 'https://www.black-ink.org/';
        $filename = 'urls.json';
        $post_category = 'Digitalia';
        $results = [];
        $total_size = 0;

        // Create Goutte Client for crawling
        $client = new Client();

        // Create and use a guzzle client instance that will timeout after 90 seconds
        $guzzleClient = new \GuzzleHttp\Client(array(
            'timeout' => 90,
            'verify' => false,
        ));
        $client->setClient($guzzleClient);

        // Send crawl request
        $crawler = $client->request('GET', $crawl_url);

        // fetch links from specified category
        $articles = $crawler->filter('article')->each(function ($article, $i) use (&$results, &$total_size, $post_category, $client) {
            $category = $article->filter('span.category');
            if ($category->text() == "Posted in: {$post_category}") {
                $list = $article->filter('div.entry-summary > ul > li')->each(function ($summary, $j) use (&$results, &$total_size, $client) {
                    $url = $summary->filter('a');
                    $url_crawler = $client->request('GET', $url->attr('href'));
                    $headers = $client->getResponse()->getHeaders();
                    $total_size += !empty($headers['Content-Length'][0]) ? $headers['Content-Length'][0] : 0;
                    $meta_description = $url_crawler->filter('meta[name=description]')->first();
                    $meta_keywords = $url_crawler->filter('meta[name=keywords]')->first();

                    $results['results'][] = [
                        'url' => $url->attr('href'),
                        'link' => $url->text(),
                        'meta_description' => $meta_description->count() ? $meta_description->attr('content') : '',
                        'keywords' => $meta_keywords->count() ? $meta_keywords->attr('content') : '',
                        'filesize' => (!empty($headers['Content-Length'][0]) ? $headers['Content-Length'][0] : 0) . 'KB',
                    ];
                });
            }
        });

        $results['total'] = $total_size . 'KB';

        Storage::disk('local')->put($filename, json_encode($results, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));

        return response()->json($results);
    }
}