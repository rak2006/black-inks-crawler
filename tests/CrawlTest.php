<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CrawlTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCrawl()
    {
        $response = $this->call('GET', '/api/v1/crawl');
        $this->assertEquals(200, $response->status());
    }
}
